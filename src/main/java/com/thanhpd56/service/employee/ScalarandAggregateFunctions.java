package com.thanhpd56.service.employee;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by PHANTHANH on 8/19/2015.
 */
public class ScalarandAggregateFunctions {

    public static void main(String[] args) {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("Eclipselink_JPA");
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        // Scalar Function (upper)
        Query query = entityManager.createQuery("SELECT upper(e.ename) from Employee e");
        List<String> employees = query.getResultList();

        for (String e : employees) {
            System.out.println(e);
        }

        // Aggregate Function (max)
        Query query1 = entityManager.createQuery("SELECT max(e.salary) from Employee  e");

        Double result = (Double) query1.getSingleResult();
        System.out.println("Max: " + result);


    }
}
