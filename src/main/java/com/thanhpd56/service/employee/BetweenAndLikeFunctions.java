package com.thanhpd56.service.employee;

import com.thanhpd56.entity.Employee;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by PHANTHANH on 8/19/2015.
 */
public class BetweenAndLikeFunctions {
    public static void main(String[] args) {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("Eclipselink_JPA");
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        // Between

        Query query = entityManager.createQuery("select e from Employee e where e.salary between 1000 and 2000");
        List<Employee> employeeList = query.getResultList();
        for (Employee e : employeeList) {
            System.out.println(e);
        }

        Query query1 = entityManager.createQuery("select e from Employee  e where e.ename like 'T%'");
        List<Employee> employeeList1 = query1.getResultList();
        for (Employee e : employeeList1) {
            System.out.println(e);
        }

    }
}
