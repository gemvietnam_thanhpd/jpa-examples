package com.thanhpd56.entity.staff_joinedtable;

import javax.persistence.*;

/**
 * Created by PHANTHANH on 8/19/2015.
 */

@Entity
@Table
@Inheritance(strategy = InheritanceType.JOINED)

public class Staff {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int sid;
    private String sname;

    public Staff() {
        super();
    }

    public Staff(int sid, String sname) {
        this.sid = sid;
        this.sname = sname;
    }

    public int getSid() {
        return sid;
    }

    public void setSid(int sid) {
        this.sid = sid;
    }

    public String getSname() {
        return sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }

    @Override
    public String toString() {
        return "[sid:" + sid + ",sname:" + sname + "]";
    }
}
