package com.thanhpd56.service.staff;

import com.thanhpd56.entity.NonTeachingStaff;
import com.thanhpd56.entity.Staff;
import com.thanhpd56.entity.TeachingStaff;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Created by PHANTHANH on 8/19/2015.
 */
public class SaveClient {
    public static void main(String[] args) {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("Eclipselink_JPA");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        // TeachingStaff Entity
        TeachingStaff teachingStaff1 = new TeachingStaff(1, "Thanh", "ND", "Math");
        TeachingStaff teachingStaff2 = new TeachingStaff(2, "Tung", "ND", "Physic");

        // NonTeachingStaff
        NonTeachingStaff nonTeachingStaff1 = new NonTeachingStaff(3, "AG", "Field1");
        NonTeachingStaff nonTeachingStaff2 = new NonTeachingStaff(4, "AG", "Field1");

        // Staff
        Staff staff = new Staff(5, "Hello");

        entityManager.persist(staff);
        entityManager.persist(teachingStaff1);
        entityManager.persist(teachingStaff2);
        entityManager.persist(nonTeachingStaff1);
        entityManager.persist(nonTeachingStaff2);


        entityManager.getTransaction().commit();
        entityManager.close();
        entityManagerFactory.close();


    }
}
