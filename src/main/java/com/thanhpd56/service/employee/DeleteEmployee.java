package com.thanhpd56.service.employee;

import com.thanhpd56.entity.Employee;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Created by PHANTHANH on 8/19/2015.
 */
public class DeleteEmployee {
    public static void main(String[] args) {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("Eclipselink_JPA");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();


        entityManager.remove(entityManager.find(Employee.class, 11));

        entityManager.getTransaction().commit();
        entityManager.close();
        entityManagerFactory.close();


    }
}
