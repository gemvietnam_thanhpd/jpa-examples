package com.thanhpd56.service.employee;

import com.thanhpd56.entity.Employee;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by PHANTHANH on 8/19/2015.
 */
public class NamedQueries {
    public static void main(String[] args) {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("Eclipselink_JPA");
        EntityManager entityManager = entityManagerFactory.createEntityManager();


        Query query = entityManager.createNamedQuery("find employee by id");
        query.setParameter("id", 11);

        List<Employee> employeeList = query.getResultList();
        for (Employee e : employeeList) {
            System.out.println(e);
        }


    }
}
