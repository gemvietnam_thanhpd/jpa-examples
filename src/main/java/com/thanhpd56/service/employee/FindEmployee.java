package com.thanhpd56.service.employee;

import com.thanhpd56.entity.Employee;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Created by PHANTHANH on 8/19/2015.
 */
public class FindEmployee {

    public static void main(String[] args) {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("Eclipselink_JPA");
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        Employee employee = entityManager.find(Employee.class, 11);
        System.out.println(employee);

        entityManager.close();
        entityManagerFactory.close();
    }
}
