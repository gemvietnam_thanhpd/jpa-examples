package com.thanhpd56.service.employee;

import com.thanhpd56.entity.Employee;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Created by PHANTHANH on 8/19/2015.
 */
public class CreateEmployee {


    public static void main(String[] args) {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("Eclipselink_JPA");
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        entityManager.getTransaction().begin();
        Employee employee = new Employee();

        // employee.setEid(11);
        employee.setEname("Thanh");
        employee.setDeg("Manager");
        employee.setSalary(1212);
        entityManager.persist(employee);

        entityManager.getTransaction().commit();

        entityManager.close();

        entityManagerFactory.close();


    }
}
