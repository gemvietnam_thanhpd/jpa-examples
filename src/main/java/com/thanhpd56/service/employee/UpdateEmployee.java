package com.thanhpd56.service.employee;

import com.thanhpd56.entity.Employee;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Created by PHANTHANH on 8/19/2015.
 */
public class UpdateEmployee {

    public static void main(String[] args) {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("Eclipselink_JPA");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        Employee employee = entityManager.find(Employee.class, new Integer(11));
        // before update
        System.out.println(employee);
        employee.setSalary(200);
        entityManager.getTransaction().commit();
        // after update
        entityManager.close();
        entityManagerFactory.close();
        System.out.println(employee);


    }
}
