package com.thanhpd56.entity.staff_joinedtable;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;

/**
 * Created by PHANTHANH on 8/19/2015.
 */
@Entity
@PrimaryKeyJoinColumn(referencedColumnName = "sid")
public class NonTeachingStaff extends Staff {
    private String areaexpertise;

    public NonTeachingStaff() {
        super();
    }

    public NonTeachingStaff(int sid, String sname, String areaexpertise) {
        super(sid, sname);
        this.areaexpertise = areaexpertise;
    }

    public String getAreaexpertise() {
        return areaexpertise;
    }

    public void setAreaexpertise(String areaexpertise) {
        this.areaexpertise = areaexpertise;
    }
}
